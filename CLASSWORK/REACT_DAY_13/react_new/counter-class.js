import React from 'react'
import Header from './Header'


export default class CounterAapp extends React.Component{


    constructor(props){
        super(props)

        
        this.onIncrement=this.onIncrement.bind(this)
        this.onDecrement=this.onDecrement.bind(this)
        
        this.state = {
            counter : 0
        }
    }

    onIncrement(){
        this.setState(p =>{
            return{
                counter : p.counter + 1
            }
        })

    }
    onDecrement(){
        this.setState(p =>{
            return{
                counter : p.counter - 1
            }
        })
    }
render(){
    return(
        <div>
        <Header title="counter app" subtitle="this is a counter app "/>
        
        <div className="counter">
         <h4>Counter : {this.state.counter}</h4>
         <button onClick ={this.onIncrement} className="btn btn-success mx-2" >+1</button>
         <button onClick ={this.onDecrement} className="btn btn-success mx-2">-1</button>

        </div>
        
        </div>
    )
}
}

