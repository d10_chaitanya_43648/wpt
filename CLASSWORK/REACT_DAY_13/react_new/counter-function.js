

import Header from './Header'
import { useState } from 'react'




const CounterApp = (props) =>{

   const [state,setState] = useState({
       counter : 0,
       counter2 : 0,
       
   })
   

   const onAdd = () => {
    setState({
       
        counter : state.counter + 1,   
        counter2 : state.counter2 ,
    })}

   const onMinus = () => {
       setState({
       
           counter : state.counter - 1,
           counter2 : state.counter2 ,
           
       })}

       const onAdd1 = () => {
        setState({
            counter : state.counter,
            counter2 : state.counter2 + 1
            
        })
       }
    
       const onMinus1 = () => {
           setState({
            counter : state.counter ,
               counter2 : state.counter2 - 1
           })}

    return(
        <div>
           <Header title="counter APP" subtitle="this is counting app for functional" />

           <div className="counter">

                <h5>Counter : {state.counter}</h5>
               <button onClick={onAdd} className="btn btn-success mx-2" >+1</button>
                <button onClick={onMinus}  className="btn btn-success mx-2">-1</button>

           </div>
           <div className="counter">

                <h5>Counter : {state.counter2}</h5>
               <button onClick={onAdd1} className="btn btn-success mx-2" >+1</button>
                <button onClick={onMinus1}  className="btn btn-success mx-2">-1</button>

           </div>
           
        </div>
    )

}








export default CounterApp