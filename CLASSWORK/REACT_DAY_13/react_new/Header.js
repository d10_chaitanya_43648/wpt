

const Header = (props) =>{
    return (
        <div class ="header">
        <h1>{props.title}</h1>
        <h5>{props.subtitle}</h5>
        </div>
    )
}

export default Header