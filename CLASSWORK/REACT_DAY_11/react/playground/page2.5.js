
let count = 0


const increase = () => {
     count += 1
     render()

}
const decrease = () => {
    if(count!=0){
     count -= 1
     render()}
     else{alert('you cant go down from here')}
}

const sqr = () => {
    count = count**2
    render()

}
const cube = () => {
    count = count**3
    render()

}
const reset = () => {
    count = 0
    render()

}

const render = () =>{
    const element = (
        <div>
            <h1>app1</h1>
            <div><strong>counter : {count}</strong></div>
            <button onClick={increase}>+1 </button>
            <button  onClick={decrease}>-1 </button>
            <hr />
            <button  onClick={sqr}>square </button>
            <button  onClick={cube}>cube </button>
            <button  onClick={reset}>reset</button>
        </div>
    )
    
    const root = document.getElementById('class1')
        
    ReactDOM.render(element,root)

}

render()