'use strict';

var personName = 'chaitanya';
var element = React.createElement(
    'div',
    null,
    React.createElement(
        'h1',
        null,
        React.createElement(
            'strong',
            null,
            'name'
        ),
        ': ',
        personName
    )
);

var root = document.getElementById('class1');

ReactDOM.render(element, root);
