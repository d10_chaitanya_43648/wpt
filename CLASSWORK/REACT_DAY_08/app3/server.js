const express = require('express')
const app = express()

const bodyparser = require('body-parser')

//user
const routerUser = require('./routes/user')

//middleware



app.use(bodyparser.json())

app.use(routerUser)

app.get('/',(request,response) =>{

    response.send('welcome to app' )
})
app.listen(3000,'0.0.0.0', () =>{
    console.log('server is listening')
})