

export const addtocart = (product = {}) =>{
    return{
        type : 'add-to-cart',
        payload : product,
    }
}

export const removefromcart = (product = {}) =>{
    return{
        type : 'remove-from-cart',
        payload : product,
    }
}