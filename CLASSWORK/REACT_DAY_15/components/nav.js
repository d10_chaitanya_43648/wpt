
import{ Link} from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

const Navigation = () => {

  const cart =  useSelector((state) => state.cart)

    return<div>
        
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container-fluid">
     <Link to="/home">
    <a className="navbar-brand" href="#">App1</a>
    </Link>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
      <Link to="/home">
      <a class="nav-link active" >Home</a>
      </Link>
    </li>

    <li className="nav-item">
      <Link to="/ProductScreen">
      <a class="nav-link active" >ProductScreen</a>
      </Link>
    </li>

    <li className="nav-item">
      <Link to="/counter">
      <a class="nav-link active" >Counter</a>
      </Link>
    </li>

    <li className="nav-item">
      <Link to="/dummycounter">
      <a class="nav-link active" >DummyCounter</a>
      </Link>
    </li>
         
      </ul>
      
    </div>
    <div className="d-flex">
    <Link to="/cart">
    <span className="cart-count">Cart[{cart.length}]</span>
    </Link>
    </div>
  </div>
</nav>
    </div>
}

export default Navigation