import counterreducer  from './counterreducer'
import cartreducer  from './cartreducers'
import {combineReducers} from 'redux'

const reducers = combineReducers({
    counter : counterreducer,
    cart : cartreducer,
})

export default reducers