import Header from '../components/Header'

import { useSelector, useDispatch } from 'react-redux'

const DummyCounter = (props) => {

    const counter=  useSelector((state) => state.counter)

    return<div>
        <Header title="dummyCounter" />
    <h1>counter value : {counter}</h1>
    </div>
}

export default DummyCounter