

import Header from '../components/Header'
import { useSelector, useDispatch } from 'react-redux'

import {increament,decreament} from '../actions/counterActions'

const CounterScreen = (props) => {

   const counter =  useSelector((state) => state.counter)

   const dispatch = useDispatch()

   const Onincrement = () =>{
       dispatch(increament())

   }
   
   const Ondecrement = () =>{
    dispatch(decreament())
   }

    return(<div>
        <Header title="Counter" />
        <h3>current counter ={counter} </h3>
        <button onClick={Onincrement} classname="btn btn-success ">+1</button>
        <button onClick ={Ondecrement} classname="btn btn-success ">-1</button>
    </div>)
}

export default CounterScreen