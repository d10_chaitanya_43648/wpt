import { useSelector } from "react-redux"
import Header from '../components/Header'
import { removefromcart } from "../actions/cart-actions"
import { useDispatch } from 'react-redux'
const Cart = (props) => {

    const cart =  useSelector((state) => state.cart)

    const dispatch = useDispatch()

    const removeFromcart = (product) =>{
        dispatch(removefromcart(product)) }

    return<div>
    
    <Header title="Cart" />
    <div className= "row">
    {cart.map(p=>{
            return(
               
                    
                    <div className="products col-3">
                    <div className="titlee">{p.title}</div>
                    <div className="id">{p.id}</div>
                    <div className="price">{p.price}</div>
                    <button
                    onclick={() =>{
                        removeFromcart(p)
                    }}
                        
                        className="btn btn-sn btn-danger btn-addtocart">RemoveFromCart</button>
                    
                    </div>
                    
                
            )
        })}
    </div>
    
</div>
}

export default Cart