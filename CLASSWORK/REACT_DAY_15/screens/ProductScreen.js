

import Header from '../components/Header'
import {addtocart,removefromcart} from '../actions/cart-actions'
import { useDispatch } from 'react-redux'
const ProductScreen = (props) => {
    const dispatch = useDispatch()

    const Products = [
    {id:1 , title: 'product1',price: 3000},
    {id:2 , title: 'product2',price: 3000},
    {id:3 , title: 'product3',price: 3000},
    {id:4 , title: 'product4',price: 3000},
    {id:5 , title: 'product5',price: 3000},
    ]

    const addTocart = (product) =>{
        dispatch(addtocart(product))

    }

    return<div>
        <Header title="Products" />
        
        <div className= "row">
        {Products.map(p=>{
                return(
                   
                        
                        <div className="products col-3">
                        <div className="titlee">{p.title}</div>
                        <div className="id">{p.id}</div>
                        <div className="price">{p.price}</div>
                        <button
                        onClick={() =>{
                            addTocart(p)
                        }}
                        className="btn btn-sn btn-success btn-addtocart">AddToCart</button>
                        </div>
                        
                    
                )
            })}
        </div>
        
    </div>
}

export default ProductScreen