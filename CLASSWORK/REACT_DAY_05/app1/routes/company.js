const express = require('express')

const pool = require('../db')

const db = require('../db')

const router = express.Router()

router.get('/company', (request, response) => {
    const statement = `select id, title, description from company`
    db.execute(statement, (error, data) => {
      response.send(data)
    })
  })
  
  router.post('/company', (request, response) => {
    const { title, description } = request.body
    const statement = `insert into company (title, description) values ('${title}','${description}')`
    db.execute(statement, (error, data) => {
      response.send(data)
    })
  })
  
  router.put('/company/:id', (request, response) => {
    const { id } = request.params
    const { title, description } = request.body
  
    const statement = `update company set title = '${title}', description = '${description}' where id = ${id}`
    db.execute(statement, (error, data) => {
      console.log(`updated category`)
      response.send('category updated')
    })
  })
  
  router.delete('/company/:id', (request, response) => {
    const { id } = request.params
  
    const statement = `delete from company where id = ${id}`
    db.execute(statement, (error, data) => {
      response.send('deleted company')
    })
  })
  module.exports=router