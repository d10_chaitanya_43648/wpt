const express = require('express')

const pool = require('../db')

const db = require('../db')

const router = express.Router()

router.get('/product', (request, response) => {
    const statement = `select id, title, description from product`
    db.execute(statement, (error, data) => {
      response.send(data)
    })
  })
  
  router.post('/product', (request, response) => {
    const { title, description } = request.body
    const statement = `insert into product (title, description) values ('${title}','${description}')`
    db.execute(statement, (error, data) => {
      response.send(data)
    })
  })
  
  router.put('/product/:id', (request, response) => {
    const { id } = request.params
    const { title, description } = request.body
  
    const statement = `update product set title = '${title}', description = '${description}' where id = ${id}`
    db.execute(statement, (error, data) => {
      console.log(`updated product`)
      response.send('product updated')
    })
  })
  
  router.delete('/product/:id', (request, response) => {
    const { id } = request.params
  
    const statement = `delete from product where id = ${id}`
    db.execute(statement, (error, data) => {
      response.send('deleted product')
    })
  })

   module.exports=router