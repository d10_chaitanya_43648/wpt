const express = require('express')

const crypto = require('crypto-js')

const db = require('../db')

const mailer = require('../mailer')

const jwt = require('jsonwebtoken')

const config = require('../config')

const router = express.Router()

router.post('/user/signup', (request,response) =>{

    const {firstName,lastName,email,password} = request.body

    const encryptedPassword = '' + crypto.SHA256(password)

    const statement = `insert into user (firstName,lastName,email,password) values ('${firstName}','${lastName}','${email}','${encryptedPassword}')`

    db.execute(statement, (error,data) =>{
        const result = {
            status: '',
        }
        if(error){
            result['status']= 'error'
            result['error']= error
        }

        else{
            result['status']= 'succes'
            result['data']= data
            mailer.sendEmail(
                'signup.html',
                'welcome to ecommerce application',
                email,
                (error, info) => {
                  response.send(result)
                }
              )
        }
        response.send(result)
    })
})

router.post('/user/signin',(request,response) =>{
    const {email,password} = request.body

    const encryptedPassword = '' + crypto.SHA256(password)

    const statement = `select * from user where email='${email}' and password='${encryptedPassword}'`

    db.execute(statement,(error,users)=>{
        const result = {
            status : ''
        }
        if(error){
            result['status'] = 'error'
            result['error'] = error
        }
        else{
            if(users.length == 0){
            result['status'] = 'error'
            result['error'] = 'user not there'}
                else{
                    const user = users[0]
                    const token = jwt.sign({id: user['id']},config.secret)
                    result['status'] = 'success'
                    result['data'] = {
                      token :  token,
                      firstName: user['firstName'],
                      lastName: user['lastName'],
                      email: user['email'],
                      phone: user['phone'],
            }
        }
        }
        response.send(result)
    })
})

router.get('/user/profile/:id',(request,response))



module.exports = router 